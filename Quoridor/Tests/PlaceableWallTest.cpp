#include "stdafx.h"
#include "CppUnitTest.h"
#include "PlaceableWall.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	TEST_CLASS(PlaceableWallTest)
	{
	public:

		TEST_METHOD(GetInstanceWithValidInput)
		{
			Position pos{ 0, 0 };
			auto direction = Piece::Direction::South;

			auto placeableWallOpt = PlaceableWall::GetInstance(pos, direction);
			Assert::IsTrue(placeableWallOpt.has_value());

			auto& placeableWall = placeableWallOpt.value().get();
			Assert::IsTrue(pos == placeableWall.GetPosition());
			Assert::IsTrue(direction == placeableWall.GetDirection());
		}

		TEST_METHOD(GetInstanceWithInvalidInput)
		{
			{
				Position pos{ 0, 0 };
				auto direction = Piece::Direction::None; // invalid direction

				auto invalidPosition = PlaceableWall::GetInstance(pos, direction);
				Assert::IsTrue(!invalidPosition.has_value());
			}

			{
				Position pos{ Piece::INVALID_POSITION }; // invalid position
				auto direction = Piece::Direction::North;

				auto invalidPosition = PlaceableWall::GetInstance(pos, direction);
				Assert::IsTrue(!invalidPosition.has_value());
			}
		}
	};
}