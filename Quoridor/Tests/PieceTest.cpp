#include "stdafx.h"
#include "CppUnitTest.h"
#include "Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	TEST_CLASS(PieceTest)
	{
	public:

		TEST_METHOD(DefaultConstructor)
		{
			Piece p;
			
			Assert::IsTrue(Piece::INVALID_POSITION == p.AccessPosition());
		}

		TEST_METHOD(ConstructorOfPosition)
		{
			Position pos{ 0, 0 };
			Piece p{ pos };
			Assert::IsTrue(pos == p.AccessPosition());
		}

		TEST_METHOD(ApplyDirectionMask)
		{
			Position pos{ 0, 0 };
			Position expected{ 1, 0 };

			Piece::ApplyDirectionMask(pos, Piece::Direction::South);
			Assert::IsTrue(expected == pos);
		}
	};
}