#include "stdafx.h"
#include "CppUnitTest.h"
#include "PiecesHandle.h"
#include "Board.h"
#include "PiecesHandle.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	TEST_CLASS(BoardTest)
	{
	public:

		TEST_METHOD(DefaultConstructor)
		{
			Board board;

			Assert::IsFalse(board.IsGamePlayable());
			Assert::IsTrue(0 == board.GetPlayerCount());
			Assert::IsTrue(board.SelectNextPlayer().has_value(), L"Must return error");
		}

		TEST_METHOD(TryAddPlayer)
		{
			Board board;
			PiecesHandle picesHandle(GameType::FourPlayers);
			const auto direction = Direction::South;

			// Add player
			Assert::IsTrue(board.TryAddPlayer(direction, picesHandle));
			// Select player
			Assert::IsFalse(board.SelectNextPlayer().has_value(), L"No error");

			auto selectedPlayer = board.GetSelectedPlayer();
			Assert::IsTrue(selectedPlayer.has_value());

			// Get origin and position of pawn
			auto& selectedPawn = selectedPlayer.value().get().GetPawn();
			auto& selectedPosition = selectedPawn.GetPosition();
			auto selectedOrigin = selectedPawn.GetOrigin();

			// Verify setup
			Assert::IsTrue(direction == selectedOrigin);
			Assert::IsTrue(Piece::ValidPosition(selectedPosition));
		}

		TEST_METHOD(MoveSelected)
		{
			Board board;
			PiecesHandle picesHandle(GameType::FourPlayers);
			const auto direction = Direction::South;

			// Add player
			board.TryAddPlayer(direction, picesHandle);
			board.SelectNextPlayer();

			const auto moveDirection = Direction::North;
			board.MoveSelected(moveDirection,
				[](auto effectiveMoveResult, auto&, auto) {
				Assert::IsTrue(Board::MoveResult::Success == effectiveMoveResult);
			});
		}

		TEST_METHOD(GetOpenSideways)
		{
			Board board;
			const auto moveDirection = Direction::South;
			const auto expectedSideway = Direction::East;

			auto openSidewaysOpt = board.GetOpenSideways({ 0, 0 }, moveDirection);
			Assert::IsTrue(openSidewaysOpt.has_value());

			auto& openSideways = openSidewaysOpt.value();
			Assert::IsTrue(openSideways.test(static_cast<int>(expectedSideway)));
		}

		TEST_METHOD(MoveSelectedDiag)
		{
			Board board;
			PiecesHandle picesHandle(GameType::FourPlayers);
			const auto direction = Direction::South;

			// Add player
			board.TryAddPlayer(direction, picesHandle);
			board.SelectNextPlayer();

			auto selectedPlayer = board.GetSelectedPlayer();
			const Pawn& selectedPawn = selectedPlayer.value().get().GetPawn();

			const auto moveDirectionOne = Direction::East;
			const auto moveDirectionTwo = Direction::North;

			auto openSidewaysOpt = board.GetOpenSideways(selectedPawn, moveDirectionOne);
			Assert::IsTrue(openSidewaysOpt.has_value(), L"The sideways");
			auto& openSideway = openSidewaysOpt.value();

			Assert::IsTrue(openSideway.test(static_cast<int>(moveDirectionTwo)), L"Second direction is open");

			board.MoveSelectedDiag(moveDirectionOne, moveDirectionTwo,
				[](auto effectiveMoveResult, auto&, auto) {
				Assert::IsTrue(Board::MoveResult::Success == effectiveMoveResult, L"Success move");
			});
		}
	};
}