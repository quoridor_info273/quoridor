#include "stdafx.h"
#include "CppUnitTest.h"
#include "PiecesHandle.h"
#include "Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	TEST_CLASS(PiecesHandleTest)
	{
	public:

		TEST_METHOD(Constructor)
		{
			{
				PiecesHandle ph{ GameType::TwoPlayers };
				Assert::IsTrue(ph.m_wallsPerPlayer == 10, L"TwoPlayers");
			}

			{
				PiecesHandle ph{ GameType::ThreePlayers };
				Assert::IsTrue(ph.m_wallsPerPlayer == 6, L"ThreePlayers");
			}

			{
				PiecesHandle ph{ GameType::FourPlayers };
				Assert::IsTrue(ph.m_wallsPerPlayer == 5, L"FourPlayers");
			}
		}

		TEST_METHOD(GetPawn)
		{
			PiecesHandle ph{ GameType::TwoPlayers };
			{
				auto direction = Direction::North;
				auto optPawn = ph.GetPawn(direction);
				Assert::IsTrue(optPawn.has_value());

				auto& pawn = optPawn.value().get();
				Assert::IsTrue(pawn.GetOrigin() == direction);
				
				{
					auto optPawn = ph.GetPawn(direction);
					Assert::IsFalse(optPawn.has_value(), L"Each pawn is unique");
				}
			}
		}

		TEST_METHOD(GetPawnWithInvalidDirection)
		{
			PiecesHandle ph{ GameType::TwoPlayers };
			auto direction = Direction::None;

			Assert::IsFalse(ph.GetPawn(direction).has_value());
		}

		TEST_METHOD(GetPlaceableWall)
		{
			PiecesHandle ph{ GameType::FourPlayers };

			auto playerOpt = Player::MakePlayer(Direction::West, ph);
			Assert::IsTrue(playerOpt.has_value());

			auto& player = playerOpt.value();
			Assert::IsTrue(ph.GetPlaceableWall(player, { 0, 0 }, Direction::North).has_value());
		}

	};
}