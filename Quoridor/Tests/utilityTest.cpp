#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\Utility\utility.h"
#include <array>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	TEST_CLASS(UtilityTest)
	{
	public:

		TEST_METHOD(stringToIntegerWithValidInput)
		{
			#define number1 234432
			#define number2 32323
			#define number3 -64564
			#define number4 +324234

			std::array<int, 4> validNumbers{ number1, number2, number3, number4 };
			std::array<string, 4> validNumbersStr{ TO_STR(number1), TO_STR(number2), TO_STR(number3), TO_STR(number4)};
			
			auto validNumbersItr = validNumbers.cbegin();
			for (const auto& validNumber : validNumbersStr) {
				auto intOpt = stringToInteger(validNumber);
				Assert::IsTrue(intOpt.has_value(), L"Should return an int.");
				Assert::IsTrue(intOpt.value() == *validNumbersItr++, L"Should return correct int");
			}
		}

		TEST_METHOD(stringToIntegerWithInvalidInput)
		{
			std::array<string, 5> invalidNumbersStr{ "43 fd", "43.3", "f 43", ".432", "23-3" };
			for (const auto& validNumber : invalidNumbersStr) {
				Assert::IsTrue(!stringToInteger(validNumber).has_value(), L"Should not return an int.");
			}
		}
	};
}