#include "stdafx.h"
#include "CppUnitTest.h"
#include "Player.h"
#include "PiecesHandle.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	TEST_CLASS(PlayerTest)
	{
	public:

		TEST_METHOD(MakePlayer)
		{
			auto direction = Piece::Direction::South;
			PiecesHandle ph{ GameType::FourPlayers };

			auto playerOpt = Player::MakePlayer(direction, ph);
			Assert::IsTrue(playerOpt.has_value());

			auto& player = playerOpt.value();
			Assert::IsTrue(direction == player.AccessPawn().GetOrigin());
		}
	};
}