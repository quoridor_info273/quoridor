#include "stdafx.h"
#include "CppUnitTest.h"
#include "PiecesHandle.h"
#include "Game.h"
#include "Board.h"
#include "PiecesHandle.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	TEST_CLASS(SearchTest)
	{
	public:

		TEST_METHOD(Search)
		{
			Board board;
			PiecesHandle picesHandle(GameType::FourPlayers);
			auto direction = Direction::South;

			// Add player
			Assert::IsTrue(board.TryAddPlayer(direction, picesHandle));
			// Select player
			Assert::IsFalse(board.SelectNextPlayer().has_value(), L"No error");
			
			auto selectedPlayer = board.GetSelectedPlayer();
			Assert::IsTrue(selectedPlayer.has_value());
			
			// Get origin and position of pawn
			auto& selectedPawn = selectedPlayer.value().get().GetPawn();
			auto& selectedPosition = selectedPawn.GetPosition();
			auto selectedOrigin = selectedPawn.GetOrigin();

			// selectedPlayer should be able to reach the other side
			Assert::IsTrue(search(board, std::ref(selectedPosition),
				[&board, selectedOrigin](const Position& passing) {
				return board.IsFinishingPosition(passing, selectedOrigin);
			}));
		}
	};
}