#include "stdafx.h"
#include "CppUnitTest.h"
#include "Pawn.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{
	TEST_CLASS(PawnTest)
	{
	public:

		TEST_METHOD(DefaultConstructor)
		{
			Pawn p;
			Assert::IsTrue(Piece::INVALID_POSITION == p.GetPosition());
			Assert::IsTrue(Piece::INVALID_DIRECTION == p.GetOrigin());
		}

		TEST_METHOD(ConstructorOfPositionAndDirection)
		{
			Position pos{ 0, 0 };
			auto origin = Piece::Direction::North;
			Pawn p{ pos, origin };

			Assert::IsTrue(pos == p.GetPosition());
			Assert::IsTrue(origin == p.GetOrigin());
		}
	};
}