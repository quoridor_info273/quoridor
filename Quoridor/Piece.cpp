#include "Piece.h"

const Position Piece::INVALID_POSITION{ -1, -1 };
const Piece::Direction Piece::INVALID_DIRECTION = Piece::Direction::None;
array<Position, Piece::DIRECTION_COUNT> Piece::m_directionMask{};

Piece::Piece() :
        m_position{INVALID_POSITION}
{
	InitDirectionMask();
}
Piece::Piece(const Position& p) :
        m_position{p.first, p.second}
{
	InitDirectionMask();
}

Position& Piece::AccessPosition() {
        return m_position;
}

const char* directionToStr(Piece::Direction direction)
{
	static const char north[] = "North";
	static const char south[] = "South";
	static const char west[] = "West";
	static const char east[] = "East";
	static const char none[] = "None";

	switch (direction)
	{
	case Piece::Direction::North:
		return north;
	case Piece::Direction::South:
		return south;
	case Piece::Direction::West:
		return west;
	case Piece::Direction::East:
		return east;
	default:
		return none;
	}
}

static void addPosition(Position& dest, const Position& other)
{
	auto&[x_dest, y_dest] = dest;
	auto&[x_other, y_other] = other;
	x_dest += x_other;
	y_dest += y_other;
}

void Piece::ApplyDirectionMask(Position& pos, Direction direction)
{
	if (Direction::None == direction)
		throw "[Debug] Cannot apply None mask";
	addPosition(pos, m_directionMask[(int)direction]);
}

const Position& Piece::GetDirectionMask(Direction direction)
{
	static Position directionNone{ 0, 0 };
	if (Direction::None == direction) {
		return directionNone;
	}
	return m_directionMask[static_cast<int>(direction)];
}

bool Piece::ValidDirection(Direction direction)
{
	return (direction != INVALID_DIRECTION) ? true : false;
}

bool Piece::ValidPosition(const Position& pos)
{
	return (pos != INVALID_POSITION) ? true : false;
}

// FIX ME
void Piece::InitDirectionMask()
{
	static bool initalized = false;

	if (initalized) return;

	m_directionMask.at((int)Direction::North).first = -1;
	m_directionMask.at((int)Direction::South).first = +1;
	m_directionMask.at((int)Direction::West).second = -1;
	m_directionMask.at((int)Direction::East).second = +1;
	initalized = true;
}

// dead code
static void assertDirectionAsIndecies()
{
	static_assert((int)Piece::Direction::North == 0);
	static_assert((int)Piece::Direction::South == 1);
	static_assert((int)Piece::Direction::West == 2);
	static_assert((int)Piece::Direction::East == 3);
	static_assert((int)Piece::Direction::None == 15);
}

int toInt(Piece::Direction direction)
{
	return static_cast<int>(direction);
}

Piece::Direction toDir(int val)
{
	using Direction = Piece::Direction;
	auto result = Direction::None;

	switch (val)
	{
	case 0:
		result = Direction::North;
		break;
	case 1:
		result = Direction::South;
		break;
	case 2:
		result = Direction::West;
		break;
	case 3:
		result = Direction::East;
	}

	if (toInt(result) != val)
		throw "Conversion failed.";

	return result;
}


Piece::Direction charToDirection(char ch)
{
	using Direction = Piece::Direction;

	switch (ch) {
	case 'w': case 'W':
		return Direction::North;
	case 'a': case 'A':
		return Direction::West;
	case 's': case 'S':
		return Direction::South;
	case 'd': case 'D':
		return Direction::East;
	case 'q': case 'Q':
		return Direction::None;
	default:
		throw "Should've not reached here.";
	}
}

const char* directionToChar(Piece::Direction direction)
{
	using Direction = Piece::Direction;

	static const char north[] = "wW";
	static const char south[] = "sS";
	static const char west[] = "aA";
	static const char east[] = "dD";

	switch (direction) {
	case Direction::North:
		return north;
	case Direction::South:
		return south;
	case Direction::West:
		return west;
	case Direction::East:
		return east;
	default:
	 	throw "Should've not reached here.";
	}
}

ostream& operator << (ostream& out, const Position& p)
{
	auto&[row, col] = p;
	return out << '(' << row << ", " << col << ')';
}