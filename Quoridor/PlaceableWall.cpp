#include "PlaceableWall.h"
#include "req.hh"

// one instance
PlaceableWall PlaceableWall::m_instance(Piece::INVALID_POSITION, Piece::INVALID_DIRECTION);

PlaceableWall::PlaceableWall(const Position& pos, Direction direction) :
	Piece(pos), m_direction(direction)
{
	/* empty */
}

void PlaceableWall::SetDirection(Direction direction)
{
	req(Piece::ValidDirection(direction));
	m_direction = direction;
}

void PlaceableWall::SetPosition(const Position& position)
{
	req(Piece::ValidPosition(position));
	m_position = position;
}

Piece::Direction PlaceableWall::GetDirection() const
{
	return m_direction;
}

const Position& PlaceableWall::GetPosition() const
{
	return m_position;
}

OptRef<PlaceableWall> PlaceableWall::GetInstance(const Position& pos, Direction direction)
{
	if (!Piece::ValidDirection(direction) || !Piece::ValidPosition(pos))
		return {};

	m_instance.m_direction = direction;
	m_instance.m_position = pos;
	return m_instance;
}