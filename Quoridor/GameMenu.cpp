#include "GameMenu.h"

#include <fstream>
using std::ifstream;

GameMenu::GameMenu()
{
}


GameMenu::~GameMenu()
{
}

variant<GameType, ErrStr> GameMenu::ParseInput(const string& line)
{
	{
		auto parseResult = applyParser(line, Parse::Quiting);
		if (std::holds_alternative<Success>(parseResult))
			return Exit("Goodbye!");
	}

	{
		auto charToGameType = [](char c) {
			switch (c)
			{
			case '2':
				return GameType::TwoPlayers;
			case '3':
				return GameType::ThreePlayers;
			case '4':
				return GameType::FourPlayers;
			default:
				return GameType::None;
			}
		};

		auto parseResult = applyParser<GameType>(line, Parse::PlayerCount,
			[charToGameType](ParsedValue<GameState::PlayerCount>& parsedValue, auto) {
			return charToGameType(parsedValue);
		});

		if (std::holds_alternative<GameType>(parseResult)) {
			auto gameType = std::get<GameType>(parseResult);
			if (GameType::None != gameType) {
				return gameType;
			}
		}
	}

	return ErrStr("Invalid option.");
}

GameResult GameMenu::OnInput(variant<GameType, ErrStr> ioResult)
{
	if (std::holds_alternative<ErrStr>(ioResult)) {
		const ErrStr& err = std::get<ErrStr>(ioResult);

		ShowMenu();
		cout << err.view() << endl;

		if (err.exit()) {
			GameMenu::logger.log(err.view().data(), Logger::Level::Warning);
			return GameResult::Exited;
		}

		GameMenu::logger.log(err.view().data(), Logger::Level::Error);
		return GameResult::InvalidInput;
	}
	
	if (std::holds_alternative<GameType>(ioResult)) {
		// get input from startGame
		{
			string msg = "Starting game of ";
			msg += GameTypeToString(std::get<GameType>(ioResult));
			GameMenu::logger.log(msg);
		}

		// testing
		//ifstream input("input.txt");
		//GameMenu::StartGame(std::get<GameType>(ioResult), input);
		
		GameMenu::StartGame(std::get<GameType>(ioResult), cin);
	}

	return GameResult::Finished;
}


void GameMenu::NewGame()
{
	GameMenu::logger.log("At game inital state.");
	ShowMenu();

	string line;
	cout << "\n\t> ";
	std::getline(cin, line);
	auto actionResult = OnInput(GameMenu::ParseInput(line));

	// read until valid input
	while (GameResult::InvalidInput == actionResult || GameResult::Exception == actionResult) {
		cout << "\nAction: ";
		std::getline(cin, line);
		actionResult = OnInput(GameMenu::ParseInput(line));
	}

	logger.log("Game exited.");
}

void GameMenu::ShowMenu()
{
	system(SYS_CLEAR);

	cout << "\n\n\n\n\n\n\n\n\n" << endl;
	print_green("\tMenu\n");
	
	cout << '\t';
	for (char c : string_view{ Parse::playerCount }) {
		cout << c << ' ';
	}
	cout << "- Start Game\n";

	cout << '\t';
	for (char c : string_view{ Parse::quiting }) {
		cout << c << ' ';
	}
	cout << "- Quit Game\n";
}

ostream& operator << (ostream& out, deque<Direction> dq)
{
	size_t i = 1;
	for (auto player : dq) {
		out << '\t' << i++ << ". " << directionToStr(player) << '\n';
	}
	return out << '\n';
}

void GameMenu::StartGame(GameType gameType, istream& in)
{
	Game game{ gameType, in };
	game.repaint();
	auto result = game.PlayerGameLoop();
	system(SYS_CLEAR);
	cout << "\n\n\n\n\n\n\n\n\n" << endl;

	GameMenu::logger.log("Game finished.");

	// If there was an error display the error
	if (std::holds_alternative<ErrStr>(result)) {
		auto& err = std::get<ErrStr>(result);
		if (!err.exit()) {
			GameMenu::logger.log(err.view().data(), Logger::Level::Error);
			cout << err.view() << endl;
		}
		GameMenu::logger.log("Exited", Logger::Level::Warning);
	}
	else {
		// print end game stats
		GameMenu::logger.log("Showing winners");
		ShowPlayerStats(std::get<PlayerStats>(result));
		system(SYS_PAUSE);
	}
	NewGame();
}

void GameMenu::ShowPlayerStats(const PlayerStats& playerStats)
{
	auto&[winners, losers, quiters] = playerStats;

	if (!winners.empty()) {
		print_green("\tWinners\n");
		cout << winners << endl;
	}
	if (!losers.empty()) {
		print_red("\tLosers\n");
		cout << losers << endl;
	}
	if (!quiters.empty()) {
		cout << "\tQuiters\n" << quiters << endl;
	}
}

ofstream GameMenu::logFile{ "logs.txt", std::ios::app };
Logger GameMenu::logger{ GameMenu::logFile };