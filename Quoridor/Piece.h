#pragma once

#include <utility>
#include <array>
#include <bitset>
#include <ostream>
using std::array, std::bitset, std::ostream, std::pair;

using Position = pair<int, int>;

class Piece {
public:
	static const size_t DIRECTION_COUNT = 4; // None excluded
	enum class Direction : char {
		North,
		South,
		West,
		East,
		None = 15
	};

	// constructors
	Piece();
	explicit Piece(const Position&);

	Position& AccessPosition();

// helpers
public:
	// declarations
	using DirectionChecklist = bitset<DIRECTION_COUNT>;

	// procedures
	static void ApplyDirectionMask(Position&, Direction);
	static const Position& GetDirectionMask(Direction);

	// flags
	static const Position INVALID_POSITION;
	static const Direction INVALID_DIRECTION;
	static bool ValidDirection(Direction);
	static bool ValidPosition(const Position&);
protected:
	Position m_position;

private:
	static array<Position, DIRECTION_COUNT> m_directionMask;
	static void InitDirectionMask();
};

// helpers
int toInt(Piece::Direction);
Piece::Direction toDir(int val);

Piece::Direction charToDirection(char);
const char* directionToChar(Piece::Direction);

const char* directionToStr(Piece::Direction);

ostream& operator << (ostream&, const Position&);