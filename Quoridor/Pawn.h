#pragma once
#include "Piece.h"
#include "req.hh"
class Pawn :
	public Piece
{
public:
	Pawn();

	Pawn(const Pawn&) = delete;
	Pawn& operator=(const Pawn&) = delete;

	Pawn(const Position&, Direction origin);

	Pawn& InitOrigin(Direction);
	Pawn& InitPosition(const Position& p);

	Direction GetOrigin() const;
	const Position& GetPosition() const;

	static Pawn dummyPawn;
private:
	Direction m_origin;
};