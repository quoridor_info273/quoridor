#include "Board.h"
#include "req.hh"
#include <iostream>
using std::cout;
variant<Piece::DirectionChecklist, Board::MoveResult> Board::UpdateStateUponPassingTrench(Pawn& passing, Direction direction)
{
	auto[x_old, y_old] = passing.AccessPosition(); // save current position

	// update to next position
	Piece::ApplyDirectionMask(passing.AccessPosition(), direction);
	auto&[x, y] = passing.AccessPosition();

	// check collision with other pawn
	if (tiles[x][y].has_value()) {
		// try move past
		if (IsAccessible(passing.GetPosition(), direction)) {
			Piece::ApplyDirectionMask(passing.AccessPosition(), direction);
		}
		else {
			x = x_old;
			y = y_old;
			if (auto openSideWays = GetOpenSideways(passing, direction)) {
				return openSideWays.value();
			}
			return MoveResult::BlockedDirection;
		}
	}

	// effectively move the pawn
	tiles[x][y].swap(tiles[x_old][y_old]);
	if (!IsGamePlayable()) {
		tiles[x][y].swap(tiles[x_old][y_old]);
		x = x_old;
		y = y_old;
		return MoveResult::InvalidDirection;
	}
	req(tiles[x][y].has_value(), "[Debug]");
	if (tiles[x_old][y_old].has_value())
		throw "[Debug]";
	req(&tiles[x][y].value().get() == &passing, "[Debug]");

	return MoveResult::Success;
}


variant<Success, Fail> Board::UpdateStateUponMovingDiag(Pawn& passing, Direction one, Direction two)
{
	auto[x_old, y_old] = passing.AccessPosition(); // save current position

	// update to next position
	if (!IsPassible(passing.GetPosition(), one)) {
		return Fail{};
	}
	Piece::ApplyDirectionMask(passing.AccessPosition(), one);

	if (!IsAccessible(passing.GetPosition(), two)) {
		return Fail{};
	}
	Piece::ApplyDirectionMask(passing.AccessPosition(), two);

	// check collision with other pawn
	auto&[x, y] = passing.AccessPosition();
	if (tiles[x][y].has_value()) {
		x = x_old;
		y = y_old;
		return Fail{};
	}

	// effectively move the pawn
	tiles[x][y].swap(tiles[x_old][y_old]);
	if (!IsGamePlayable()) {
		tiles[x][y].swap(tiles[x_old][y_old]);
		x = x_old;
		y = y_old;
		return Fail{};
	}

	req(tiles[x][y].has_value(), "[Debug]");
	req(!tiles[x_old][y_old].has_value(), "[Debug]");
	req(&tiles[x][y].value().get() == &passing, "[Debug]");

	return Success{};
}

optional<Piece::DirectionChecklist> Board::GetOpenSideways(Position position, Direction direction) const
{
	Piece::DirectionChecklist openSideways{};
	
	Piece::ApplyDirectionMask(position, direction);
	bool any = false;

	// maybe iterate over directions
	for (int itr = 0; itr < openSideways.size(); ++itr) {
		Direction temp = static_cast<Direction>(itr);

		// IsIntersection checks to see if direction and directionItr are intersecting direction.
		// For example: N & E are intersecting;  N & S are not intersecting.
		if (IsIntersection(direction, temp) && IsAccessible(position, temp)) {
			openSideways.set(itr);
			any = true;
		}
	}

	if (any) {
		return openSideways;
	}
	return {};
}

// todo: move calls to this function to the returned function
optional<Piece::DirectionChecklist> Board::GetOpenSideways(const Pawn& pawn, Direction direction) const
{
	return GetOpenSideways(pawn.GetPosition(), direction);
}


variant<Board::MoveResult, Piece::DirectionChecklist> Board::PassTrench(Pawn& pawn, Direction direction)
{
	auto&[x, y] = pawn.AccessPosition();


	if (IsPassible(pawn.GetPosition(), direction)) {
		auto result = UpdateStateUponPassingTrench(pawn, direction);
		if (std::holds_alternative<MoveResult>(result)) {
			return std::get<MoveResult>(result);
		}
		return std::get<Piece::DirectionChecklist>(result);
	}
	return MoveResult::BlockedDirection;
}

DirectionChecklistErrOpt Board::MoveSelected(Direction direction, function<void(MoveResult, Pawn&, Direction)> postMoveAction)
{
	auto selected = GetSelectedPlayerNonConst();
	req(Piece::ValidDirection(direction));

	if (!selected.has_value()) {
		throw "[Debug] This function works only after selecting a player.";
	}
	auto& pawn = selected.value().get().AccessPawn();
	
	auto result = PassTrench(pawn, direction);
	if (std::holds_alternative<MoveResult>(result)) {
		postMoveAction(std::get<MoveResult>(result), pawn, direction);
		return {};
	}
	return std::get<Piece::DirectionChecklist>(result);
}

// later
template<typename T, typename C>
std::enable_if_t<std::is_convertible_v<T, C>, C&>
convert(T& t) {
	return static_cast<C&>(t);
}

void Board::MoveSelectedDiag(Direction one, Direction two, function<void(MoveResult, Pawn&, Direction)> postMoveAction)
{
	auto selected = GetSelectedPlayerNonConst();

	if (!selected.has_value()) {
		throw "[Debug] This function works only after selecting a player.";
	}
	auto& pawn = selected.value().get().AccessPawn();

	req(Piece::ValidDirection(one));
	req(Piece::ValidDirection(two));

	auto result = UpdateStateUponMovingDiag(pawn, one, two);
	if (std::holds_alternative<Success>(result)) {
		postMoveAction(MoveResult::Success, pawn, two);
	}
	else {
		postMoveAction(MoveResult::BlockedDirection, pawn, two);
	}
}

// last in this file
void Board::SetupPlayer(Player& player, int lineIndex)
{
	auto& pawn = player.AccessPawn();
	req(!Piece::ValidPosition(pawn.GetPosition()), "Unplaced pawn should have invalid position.");
	req(Piece::ValidDirection(pawn.GetOrigin()), "[Debug]");

	req(lineIndex < tiles.size());

	int effectiveIndex = static_cast<int>(pawn.GetOrigin());
	// TODO take ph& as arg, and use m_usedPawn from there also?.
	req(!m_usedPawns.test(effectiveIndex), "One player per pawn type");
	m_usedPawns.set(effectiveIndex);
	auto&[row, col] = pawn.AccessPosition();

	switch (pawn.GetOrigin())
	{
	case Direction::North:
		row = 0;
		col = lineIndex;
		break;
	case Direction::South:
		row = static_cast<int>(tiles.size()) - 1;
		col = lineIndex;
		break;
	case Direction::East:
		row = lineIndex;
		col = static_cast<int>(tiles.size()) - 1;
		break;
	case Direction::West:
		row = lineIndex;
		col = 0;
		break;
	}

	m_players[static_cast<int>(pawn.GetOrigin())].emplace(player); // std::move
	++m_playerCount;
	tiles[row][col].emplace(pawn);
}

Direction nextDirection(Direction direction)
{
	if (Direction::None == direction) {
		return toDir(0);
	}

	auto nextIndex = toInt(direction) + 1;

	if (nextIndex >= Piece::DIRECTION_COUNT) {
		return toDir(0);
	}

	return toDir(nextIndex);
}

optional<ErrStr> Board::SelectNextPlayer(Direction origin)
{
	if (!Piece::ValidDirection(origin)) {
		origin = GetSelectedOrigin();
	}

	if (GetPlayerCount() < 1) {
		return ErrStr("No next player.");
	}

	do {
		origin = nextDirection(origin);
		if (!Piece::ValidDirection(origin))
			throw "[Debug] nextDirection returned None.";
	} while (!HasPlayer(origin));

	if (auto result = TrySelectPlayer(origin)) {
		return {};
	}
	
	throw "[Debug] HasPlayer => TrySelectPlayer should work.";
}

bool Board::TrySelectPlayer(Direction origin)
{
	if (Direction::None == origin) {
		return false;
	}

	if (!HasPlayer(origin)) {
		return false;
	}

	SetSelectedPlayer(origin);
	
	auto result = GetSelectedPlayerNonConst();
	if (!result.has_value()) {
		throw "[Debug] ?";
	}

	return true;
}

bool Board::TryAddPlayer(Direction origin, PiecesHandle& ph)
{
	if (auto playerOpt = Player::MakePlayer(origin, ph)) {
		static int middleIndex = (static_cast<int>(tiles.size()) - 1) / 2;
		SetupPlayer(playerOpt.value(), middleIndex);
		return true;
	}
	return false;
}

size_t Board::GetPlayerCount() const
{
	return m_playerCount;
}

bool Board::GameFinished() const
{
	return GetPlayerCount() < 2;
}

void Board::QuitPlayer(const Player& player)
{
	auto& pawn = player.GetPawn();
	if (!HasPlayer(pawn.GetOrigin())) {
		return;
	}

	if (!IsPawnSynced(pawn))
		throw "[Debug]";
	
	int effectiveIndex = toInt(pawn.GetOrigin());

	if (!HasPlayerFinished(player)) {
		auto&[row, col] = pawn.GetPosition();
		tiles[row][col].reset();
		m_players[effectiveIndex].emplace(Player::dummyPlayer);
	}

	--m_playerCount;
	m_usedPawns.reset(effectiveIndex);
	m_players[effectiveIndex].reset();
}

Direction Board::GetSelectedOrigin() const
{
	return m_selectedPlayerOrigin;
}

bool Board::HasPlayer(Direction direction) const
{
	if (Piece::INVALID_DIRECTION == direction)
		return false;
	return m_usedPawns.test(static_cast<int>(direction));
}

// tested :)
bool Board::HasSelected() const
{
	return HasPlayer(GetSelectedOrigin());
}

bool Board::CanSelectedPlace() const
{
	if (!HasSelected())
		return false;
	if (auto selectedOpt = GetSelectedPlayer()) {
		return selectedOpt.value().get().HasWalls();
	}
	return false;
}

OptRef<const Player> Board::GetSelectedPlayer() const
{
	if (!HasSelected()) {
		return {};
	}

	// !do not copy
	auto& selectedOpt = m_players[static_cast<int>(GetSelectedOrigin())];

	if (!selectedOpt.has_value()) {
		throw "[Debug] Selected player is missing from m_players";
	}

	const auto& pawn = selectedOpt.value().GetPawn();

	if (!IsPawnSynced(pawn)) {
		throw "[Debug] Selected player must always be synced.";
	}

	return selectedOpt.value();
}

OptRef<Player> Board::GetSelectedPlayerNonConst()
{
	if (!HasSelected()) {
		return {};
	}

	// !do not copy
	auto& selectedOpt = m_players[static_cast<int>(GetSelectedOrigin())];

	if (!selectedOpt.has_value()) {
		throw "[Debug] Selected player is missing from m_players";
	}

	const auto& pawn = selectedOpt.value().AccessPawn();
	
	if (!IsPawnSynced(pawn)) {
		throw "[Debug] Selected player must always be synced.";
	}

	return selectedOpt.value();
}

void Board::SetSelectedPlayer(Direction direction)
{
	if (Direction::None == direction)
		throw "[Debug] Cannot select direction none";

	m_selectedPlayerOrigin = direction;
}

void Board::PlaceWall(PiecesHandle& ph, Position& position, Direction direction, function<void(optional<ErrStr>)> proc)
{
	auto selected = GetSelectedPlayerNonConst();

	if (!selected.has_value()) {
		throw "[Debug] This function works only after selecting a player.";
	}

	auto& player = selected.value().get();
	auto& pawn = player.AccessPawn();

	if (!player.WallCount()) {
		proc(ErrStr("No more walls available for selected player"));
		return;
	}

	if (!Piece::ValidDirection(direction)) {
		proc(ErrStr("Invalid direction for wall."));
		return;
	}

	auto placeableWallOpt = ph.GetPlaceableWall(player, position, direction);
	if (!placeableWallOpt.has_value())
		throw "[Debug]";

	auto& placeableWall = placeableWallOpt.value().get();

	bool playable = true;
	bool fits = WAllFitsDo(placeableWall,
		[&](auto oneHalf, auto otherHalf) {
		auto&[row, col] = oneHalf;
		auto&[other_row, other_col] = otherHalf;

		switch (placeableWall.GetDirection())
		{
		case Direction::North: case Direction::South:
			trenchWestEast.OccupyTrench(row, col);
			trenchWestEast.OccupyTrench(other_row, other_col);
			if (!IsGamePlayable()) {
				trenchWestEast.FreeTrench(row, col);
				trenchWestEast.FreeTrench(other_row, other_col);
				playable = false;
			}
			break;

		case Direction::West: case Direction::East:
			trenchNorthSouth.OccupyTrench(row, col);
			trenchNorthSouth.OccupyTrench(other_row, other_col);
			if (!IsGamePlayable()) {
				trenchNorthSouth.FreeTrench(row, col);
				trenchNorthSouth.FreeTrench(other_row, other_col);
				playable = false;
			}
			break;
		default:
			break;
		}
	});


	if (!playable) {
		proc(ErrStr{ "Cannot place wall there. You are blocking another player." });
		return;
	}

	if (!fits) {
		proc(ErrStr{ "Cannot place wall there." });
		return;
	}

	player.decrementWallCount();
	proc({});
}

bool Board::IsGamePlayable()
{
	const auto selectedOrigin = GetSelectedOrigin();
	
	if (!HasSelected()) {
		return false;
	}

	bool result = true;

	auto currentOrigin = selectedOrigin;
	do {
		if (auto selectedOpt = GetSelectedPlayerNonConst()) {
			auto& selectedPawn = selectedOpt.value().get().GetPawn();
			auto& selectedPosition = selectedPawn.GetPosition();

			result = search(*this, std::ref(selectedPosition),
				[this, currentOrigin](const Position& passing) {
				return IsFinishingPosition(passing, currentOrigin);
			});

			SelectNextPlayer();
			currentOrigin = GetSelectedOrigin();
		}
		else {
			result = false;
		}

	} while (selectedOrigin != currentOrigin && result);

	SetSelectedPlayer(selectedOrigin);
	return result;
}

optional<tuple<ConstPosRef, ConstPosRef, Position>> Board::GetConvenientHalfs(const Position& firstHalf, const Position& secondHalf, Direction direction) const
{
	tuple<ConstPosRef, ConstPosRef, Position> result{ firstHalf, secondHalf, {}};
	auto&[convenientFirstHalf, convenientSecondHalf, checkingHalf] = result;
	
	// get first half
	switch (direction)
	{
	case Piece::Direction::North: case Piece::Direction::West:
		convenientFirstHalf = secondHalf;
		convenientSecondHalf = firstHalf;
		break;
	case Piece::Direction::None:
		throw "[Debug] Wall cannot have Direction::None (GetPlaceableWall always returns a 'valid' wall";
	}

	// get checking half
	switch (direction)
	{
	case Piece::Direction::North: case Piece::Direction::South:
		if (auto optPosition = PositionAt(convenientFirstHalf, Direction::East)) {
			checkingHalf = optPosition.value().get();
			break;
		}
		else {
			return {};
		}
	case Piece::Direction::West: case Piece::Direction::East:
		if (auto optPosition = PositionAt(convenientFirstHalf, Direction::South)) {
			checkingHalf = optPosition.value().get();
			break;
		}
		else {
			return {};
		}
	default:
		throw "[Debug] Wall cannot have Direction::None (GetPlaceableWall always returns a 'valid' wall";
	}

	return result;
}

bool Board::WAllFitsDo(const PlaceableWall& placeableWall, function<void(const Position&, const Position&)> onFit) const
{
	auto direction = placeableWall.GetDirection();
	auto& firstHalf = placeableWall.GetPosition();

	Position secondHalf(firstHalf);
	Piece::ApplyDirectionMask(secondHalf, direction);

	if (auto halfs = GetConvenientHalfs(firstHalf, secondHalf, direction)) {
		auto [convenientFirstHalf, convenientSecondHalf, checkingHalf] = halfs.value();

		auto&[fh_row, fh_col] = convenientFirstHalf.get();
		auto&[sh_row, sh_col] = convenientSecondHalf.get();
		auto&[cheking_row, checking_col] = checkingHalf;

		// check if trench is free
		switch (direction)
		{
		case Direction::North: case Direction::South:
			if (// current axsis
				trenchWestEast.AtForward(fh_row, fh_col) ||
				trenchWestEast.AtForward(sh_row, sh_col)
				|| // intersectin check; TODO At(const Position&)
				(trenchNorthSouth.AtForward(fh_row, fh_col) &&
					trenchNorthSouth.AtForward(cheking_row, checking_col))
				) {
				return false;
			}
			break;

		case Direction::West: case Direction::East:
			if (// current axsis
				trenchNorthSouth.AtForward(fh_row, fh_col) ||
				trenchNorthSouth.AtForward(sh_row, sh_col)
				|| // intersectin check
				(trenchWestEast.AtForward(fh_row, fh_col) &&
					trenchWestEast.AtForward(cheking_row, checking_col))
				) {
				return false;
			}
			break;
		}
		onFit(placeableWall.GetPosition(), secondHalf);
		return true;
	}
	return false;
}

bool Board::HasPlayerFinished(const Player& player) const
{
	auto& pawn = player.GetPawn();
	return IsFinishingPosition(pawn.GetPosition(), pawn.GetOrigin());
}

bool Board::IsFinishingPosition(const Position& position, Direction pawnOrigin) const
{
	auto&[row, col] = position;

	int lowestIndex = 0;
	int highestIndex = static_cast<int>(tiles.size()) - 1;

	req(highestIndex > 0, "[Debug]");

	switch (pawnOrigin)
	{
	case Piece::Direction::North:
		if (row == highestIndex)
			return true;
		return false;
	case Piece::Direction::South:
		if (row == lowestIndex)
			return true;
		return false;
	case Piece::Direction::West:
		if (col == highestIndex)
			return true;
		return false;
	case Piece::Direction::East:
		if (col == lowestIndex)
			return true;
		return false;
	default:
		return false;
	}
}

bool Board::IsIntersection(Direction one, Direction other)
{
	auto&[x_one, y_one] = Piece::GetDirectionMask(one);
	auto&[x_other, y_other] = Piece::GetDirectionMask(other);

	int vectorProduct = (x_one * x_other) + (y_one * y_other);
	return 0 == vectorProduct;
}

bool Board::IsPassible(const Position& pawnPosition, Direction direction) const
{
	auto&[x, y] = pawnPosition;

	switch (direction) {
	case Direction::North:
		if (!trenchNorthSouth.AtBackward(x, y)) {
			return true;
		}
		return false;
	case Direction::West:
		if (!trenchWestEast.AtBackward(x, y)) {
			return true;
		}
		return false;
	case Direction::South:
		if (!trenchNorthSouth.AtForward(x, y)) {
			return true;
		}
		return false;
	case Direction::East:
		if (!trenchWestEast.AtForward(x, y)) {
			return true;
		}
	}
	return false;
}

bool Board::IsAccessible(Position pawnPosition, Direction direction) const
{
	if (!IsPassible(pawnPosition, direction))
		return false;

	Piece::ApplyDirectionMask(pawnPosition, direction);
	const auto&[x, y] = pawnPosition;

	bool isTargetedTileOpen = !tiles[x][y].has_value();
	return isTargetedTileOpen;
}

bool Board::IsPawnSynced(const Pawn& pawn) const
{
	if (!Piece::ValidDirection(pawn.GetOrigin()) ||
		!Piece::ValidPosition(pawn.GetPosition()))
		return false;

	auto&[row, col] = pawn.GetPosition();

	if (!tiles[row][col].has_value())
		return false;

	return &tiles[row][col].value().get() == &pawn;
}

char directionToTag(Direction direction)
{
	switch (direction)
	{
	case Direction::North:
		return 'N';
	case Direction::South:
		return 'S';
	case Direction::West:
		return 'W';
	case Direction::East:
		return 'E';
	default:
		return 'x';
	}
}

ostream& operator<<(ostream& out, const Board& board)
{
	auto& tiles = board.tiles;
	auto& trenchWestEast = board.trenchWestEast;
	auto& trenchNorthSouth = board.trenchNorthSouth;
	const char* temp = nullptr;
	// IDEA: implement range
	for (size_t row = 0; row < tiles.size(); ++row) {
		for (size_t col = 0; col < tiles.size(); ++col) {
			if (tiles[row][col].has_value()) {
				auto playerOrigin = tiles[row][col].value().get().GetOrigin();
				out << ' ' << directionToTag(playerOrigin) << ' ';
			}
			else {
				out << "   ";
			}


			if (col + 1 < tiles.size()) {
				temp = trenchWestEast.AtForward(row, col) ? "|" : ":";
				out << temp;
			}
		}
		out << '\n';
		if (row + 1 < tiles.size()) {
			for (size_t col = 0; col < trenchNorthSouth.Width(); ++col) {
				temp = trenchNorthSouth.AtForward(row, col) ? "----" : "... ";
				out << temp;
			}
			out << '\n';
		}
	}
	return out;
}


// Todo: Get neighbors(Position, Direction pawnOrigin)


OptRef<const Position> Board::PositionAt(const Position& position, Direction direction)
{
	Position nextPosition{position};
	auto& [next_row, next_col] = nextPosition;
	auto& [row_mask, col_mask] = Piece::GetDirectionMask(direction);

	next_row += row_mask;
	next_col += col_mask;

	if (next_row >= positions.size() || next_col >= positions[next_row].size() ||
		next_row < 0 || next_col < 0) {
		return {};
	}

	return positions[next_row][next_col];
}

list<ref_wrapper<const Position>> Board::GetNeighbors(const Position& position) const
{
	list<ref_wrapper<const Position>> neighbors;

	// maybe iterate over directions
	for (int itr = 0; itr < Piece::DIRECTION_COUNT; ++itr) {
		Direction directionItr = toDir(itr);

		if (IsAccessible(position, directionItr)) {
			if (auto neighborOpt = PositionAt(position, directionItr)) {
				neighbors.push_front(neighborOpt.value());
			}
			else {
				throw "[Debug] if IsAccessible => PositionAt must return a value";
			}
		}
		else if (IsPassible(position, directionItr)) {
			// pass in the current direction
			Position temp{ position };
			Piece::ApplyDirectionMask(temp, directionItr);

			// check if after pass tile is opened
			if (IsAccessible(temp, directionItr)) {
				if (auto neighborOpt = PositionAt(temp, directionItr)) {
					neighbors.push_front(neighborOpt.value());
				}
				else {
					throw "[Debug] if IsAccessible => PositionAt must return a value";
				}
			}
			else if (auto openSideWaysOpt = GetOpenSideways(position, directionItr)) {
				auto& openSideWays = openSideWaysOpt.value();
				
				// iterate over openSideWays
				for (int sidewaysItr = 0; sidewaysItr < openSideWays.size(); ++sidewaysItr) {
					if (openSideWays.test(sidewaysItr)) {


						// after move get sideway neighbor
						if (auto neighborOpt = PositionAt(temp, toDir(sidewaysItr))) {
							neighbors.push_front(neighborOpt.value());
						}
						else {
							throw "[Debug] if IsAccessible => PositionAt must return a value";
						}
					}
				}
			}

		}
	}
	return neighbors;
}

const Position& Board::GetNeighbor(const ref_wrapper<const Position>& wrapper) const
{
	return wrapper.get();
}

Board::Board() :
	m_usedPawns{},
	m_players{},
	m_playerCount{},
	m_selectedPlayerOrigin{ Piece::INVALID_DIRECTION }
{
	initPositions();
	
	// empty; maybe add ph aswell
	//
}

matrix<Position, Board::WIDTH> Board::positions;
void Board::initPositions()
{
	// range(0, n)
	for (int row = 0; row < positions.size(); ++row) {
		for (int col = 0; col < positions[row].size(); ++col) {
			auto&[rowRef, colRef] = positions[row][col];
			rowRef = row;
			colRef = col;
		}
	}
}

void Board::PrintWithLables(ostream& out) const
{
	const char* temp = nullptr;
	// IDEA: implement range
	for (size_t row = 0; row < tiles.size(); ++row) {
		for (size_t col = 0; col < tiles.size(); ++col) {
			if (tiles[row][col].has_value()) {
				auto playerOrigin = tiles[row][col].value().get().GetOrigin();
				char tag[] = { directionToTag(playerOrigin), '\0' };
				out << ' ';

				if (GetSelectedOrigin() == playerOrigin)
					print_red(tag);
				else if (HasPlayer(playerOrigin))
					out << tag;
				else
					print_green(tag);
				out << ' ';
			}
			else {
				out << "   ";
			}


			if (col + 1 < tiles.size()) {
				temp = trenchWestEast.AtForward(row, col) ? "|" : ":";
				out << temp;
			}
		}
		out << '\n';
		if (row + 1 < tiles.size()) {
			for (size_t col = 0; col < trenchNorthSouth.Width(); ++col) {
				temp = trenchNorthSouth.AtForward(row, col) ? "--- " : "... ";
				out << temp;
			}
			out << '\n';
		}
	}
}


void Board::PrintWithLablesNorthSouth(ostream& out) const
{
	const char* temp = nullptr;
	// IDEA: implement range
	for (size_t row = 0; row < tiles.size(); ++row) {
		for (size_t col = 0; col < tiles.size(); ++col) {
			if (tiles[row][col].has_value()) {
				auto playerOrigin = tiles[row][col].value().get().GetOrigin();
				char tag[] = { directionToTag(playerOrigin), '\0' };
				out << ' ';

				if (GetSelectedOrigin() == playerOrigin)
					print_red(tag);
				else if (HasPlayer(playerOrigin))
					out << tag;
				else
					print_green(tag);
				out << ' ';
			}
			else {
				out << "   ";
			}

			// if not on the board right edge
			if (col + 1 < tiles.size()) {
				temp = trenchWestEast.AtForward(row, col) ? "|" : ":";
				out << temp;
			}
			else {
				out << "   " << row;
			}
		}
		out << '\n';
		if (row + 1 < tiles.size()) {
			for (size_t col = 0; col < trenchNorthSouth.Width(); ++col) {
				temp = trenchNorthSouth.AtForward(row, col) ? "--- " : "... ";
				out << temp;
			}
			out << '\n';
		}
		else {
			out << '\n';
			for (size_t col = 0; col < trenchNorthSouth.Width() - 1; ++col) {
				out << "   " << col;
			}
			out << '\n';
		}
	}
}

void Board::PrintWithLablesWestEast(ostream& out) const
{
	const char* temp = nullptr;
	// IDEA: implement range
	for (size_t row = 0; row < tiles.size(); ++row) {
		for (size_t col = 0; col < tiles.size(); ++col) {
			if (tiles[row][col].has_value()) {
				auto playerOrigin = tiles[row][col].value().get().GetOrigin();
				char tag[] = { directionToTag(playerOrigin), '\0' };
				out << ' ';

				if (GetSelectedOrigin() == playerOrigin)
					print_red(tag);
				else if (HasPlayer(playerOrigin))
					out << tag;
				else
					print_green(tag);
				out << ' ';
			}
			else {
				out << "   ";
			}

			// if not on the board right edge
			if (col + 1 < tiles.size()) {
				temp = trenchWestEast.AtForward(row, col) ? "|" : ":";
				out << temp;
			}
		}
		out << '\n';
		if (row + 1 < tiles.size()) {
			for (size_t col = 0; col < trenchNorthSouth.Width(); ++col) {
				temp = trenchNorthSouth.AtForward(row, col) ? "--- " : "... ";
				out << temp;
			}
			out << " " << row << '\n';
		}
		else {
			out << '\n';
			for (size_t col = 0; col < trenchNorthSouth.Width(); ++col) {
				out << " " << col << "  ";
			}
			out << '\n';
		}
	}
}

