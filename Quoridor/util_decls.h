#pragma once

#include "using_functional.h"
#include "using_optional.h"
#include "using_variant.h"
#include "util_templates.h"
#include "..\Utility\utility.h"

// tags
struct Success {};
struct Fail {};

template <typename T>
using OptRef = optional<ref_wrapper<T>>;

enum class GameType : char {
	TwoPlayers = 2,
	ThreePlayers = 3,
	FourPlayers = 4,
	None
};

static inline const char* GameTypeToString(GameType gt)
{
	static const char twoPlayersStr[] = "TwoPlayers";
	static const char threePlayersStr[] = "ThreePlayers";
	static const char fourPlayersStr[] = "FourPlayers";

	switch (gt)
	{
	case GameType::TwoPlayers:
		return twoPlayersStr;
	case GameType::ThreePlayers:
		return threePlayersStr;
	case GameType::FourPlayers:
		return fourPlayersStr;
	default:
		throw "[Debug]";
	}
}

#ifdef _WIN32
#define SYS_CLEAR "cls"
#define SYS_PAUSE "pause"
#else
#define SYS_CLEAR "clear"
#define SYS_PAUSE ""
#endif