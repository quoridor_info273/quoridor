#pragma once

#include "Game.h"
#include "../Logging/Logging.h"
#include <fstream>
using std::ofstream;

// used by OnInput to show state
enum class GameResult : char
{
	Exited,
	Finished,
	InvalidInput,
	Exception
};

class GameMenu
{
public:
	GameMenu();

	static void StartGame(GameType, istream& in = cin);
	static void NewGame();
	static void ShowMenu();
	static void ShowPlayerStats(const PlayerStats&);

	static ofstream logFile;
	static Logger logger;
	
	// utilities
	static variant<GameType, ErrStr> ParseInput(const string& line);
	static GameResult OnInput(variant<GameType, ErrStr>);

	~GameMenu();
};

