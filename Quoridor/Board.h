#pragma once

#include "using_matrix.h"
#include "Pawn.h"
#include "Trench.h"
#include "util_decls.h"
#include "Player.h"
#include "Search.h"
#include <ostream>
#include <string>
#include <list>
#include <algorithm>
#include <tuple>
#include <queue>
using std::tuple, std::pair, std::list, std::string, std::ostream;

/* IDEA: implement option<T> : enum { Some<T>, Err } <=> (somewhat) variant<T, Err> */

using Direction = Piece::Direction;
using DirectionChecklist = Piece::DirectionChecklist;
using DirectionChecklistErrOpt = optional<Err<DirectionChecklist>>;

using ConstPosRef = ref_wrapper<const Position>;
using Wrapper = ref_wrapper<const Position>;

class Board : public GraphInterface<Position, ref_wrapper<const Position>>
{
public:
	enum class MoveResult : char {
		InvalidDirection,
		BlockedDirection,
		Success
	};
	Board();

	// Ideea: make_position<1>(x, y)
	// Move in straight line
	DirectionChecklistErrOpt MoveSelected(Piece::Direction, function<void(MoveResult, Pawn&, Direction)>);
	variant<MoveResult, Piece::DirectionChecklist> PassTrench(Pawn&, Direction);
	variant<Piece::DirectionChecklist, Board::MoveResult> UpdateStateUponPassingTrench(Pawn&, Direction);
	
	// Diagonal move
	void MoveSelectedDiag(Direction one, Direction two, function<void(MoveResult, Pawn&, Direction)>);
	variant<Success, Fail> UpdateStateUponMovingDiag(Pawn& passing, Direction one, Direction two);
	optional<Piece::DirectionChecklist> GetOpenSideways(Position, Direction) const;
	optional<Piece::DirectionChecklist> GetOpenSideways(const Pawn&, Direction) const;
	
	// Print
	friend ostream& operator << (ostream&, const Board&);
	void PrintWithLables(ostream& out) const;
	void PrintWithLablesNorthSouth(ostream& out) const;
	void PrintWithLablesWestEast(ostream& out) const;

	// Try placing wall
	void PlaceWall(PiecesHandle&, Position&, Direction, function<void(optional<ErrStr>)>);
	bool WAllFitsDo(const PlaceableWall&, function<void(const Position&, const Position&)>) const;
	optional<tuple<ConstPosRef, ConstPosRef, Position>> GetConvenientHalfs(const Position&, const Position&, Direction) const;

	// Game state checkers
	bool IsGamePlayable();
	size_t GetPlayerCount() const;
	bool GameFinished() const;

	// Trench Helpers
	bool IsAccessible(Position, Direction) const;
	bool IsPassible(const Position&, Direction) const;
	static bool IsIntersection(Direction one, Direction other);
	
	// Player helpers
	void SetupPlayer(Player& pawn, int lineIndex); // Player&&
	bool TryAddPlayer(Direction origin, PiecesHandle&);
	void QuitPlayer(const Player&);
	bool HasPlayer(Direction) const;
	bool HasPlayerFinished(const Player&) const;
	bool IsFinishingPosition(const Position&, Direction pawnOrigin) const;
	bool IsPawnSynced(const Pawn&) const;
	
	// Selected player helpers
	bool TrySelectPlayer(Direction origin);
	optional<ErrStr> SelectNextPlayer(Direction = Direction::None);
	bool HasSelected() const;
	Direction GetSelectedOrigin() const;
	bool CanSelectedPlace() const;
	OptRef<const Player> GetSelectedPlayer() const;
	
	// 
	static OptRef<const Position> PositionAt(const Position&, Direction);
	
	// GraphInterface implemenation
	list<ref_wrapper<const Position>> GetNeighbors(const Position&) const override;
	const Position& GetNeighbor(const ref_wrapper<const Position>& wrapper) const override;

	// board dimension
	static const size_t WIDTH = 9;
	static const size_t HEIGHT = 9;

private:
	array<optional<Player>, PiecesHandle::MAX_PLAYERS> m_players;
	
	// status
	Piece::DirectionChecklist m_usedPawns;
	Direction m_selectedPlayerOrigin;
	size_t m_playerCount;
	
	// setter/getter for m_selectedPlayerOrigin;
	void SetSelectedPlayer(Direction);
	OptRef<Player> GetSelectedPlayerNonConst();
	
public:
	/* (https://en.cppreference.com/w/cpp/language/definition):
	   static const __id__; // declared but not defined
	   inline static const __id__; // defined
	   static const __id__ = __value__; // defined (implicitly inlined)
	   -----
	   https://stackoverflow.com/questions/29397864/why-does-constexpr-static-member-of-type-class-require-a-definition
	   -----
	 */
private:
	// helpers
	static void initPositions();
	static matrix<Position, WIDTH> positions;

private:
	// the board
	matrix<OptRef<Pawn>, WIDTH> tiles;
	// the trenches
	Trench<WIDTH - 1, HEIGHT> trenchNorthSouth;
	Trench<WIDTH, HEIGHT - 1> trenchWestEast;
};

// Need for unordered_map<Wrapper> (used in "Search.h")s
// from https://www.nullptr.me/2018/01/15/hashing-stdpair-and-stdtuple/
template<typename T>
void hash_combine(std::size_t& seed, T const& key)
{
	seed ^= std::hash<T>{}(key)+0x9e3779b9 + (seed << 6) + (seed >> 2);
}

namespace std {
	template<>
	struct less<Wrapper> {
		inline bool operator()(const Wrapper& lhs, const Wrapper& rhs) const {
			return lhs.get() < rhs.get();
		}
	};

	template<>
	struct equal_to<Wrapper> {
		inline bool operator()(const Wrapper& lhs, const Wrapper& rhs) const {
			auto&[l_row, l_col] = lhs.get();
			auto&[r_row, r_col] = rhs.get();
			return l_row == r_row && l_col == r_col;
		}
	};

	template<typename S, typename T>
	struct hash<pair<S, T>>
	{
		inline size_t operator()(const pair<S, T>& val) const
		{
			size_t seed = 0;
			hash_combine(seed, val.first);
			hash_combine(seed, val.second);
			return seed;
		}
	};

	template<>
	struct hash<Wrapper> {
		inline size_t operator()(const Wrapper& w) const {
			return hash<Position>{}(w.get());
		}
	};
}