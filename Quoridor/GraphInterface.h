#pragma once

#include <list>
using std::list;

template<typename T, typename Wrapper = T>
class GraphInterface
{
public:
	virtual list<Wrapper> GetNeighbors(const T&) const = 0;
	virtual const T& GetNeighbor(const Wrapper&) const = 0;
	virtual ~GraphInterface() {};
};