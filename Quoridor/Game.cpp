#include "Game.h"
#include "GameMenu.h"
#include "Search.h"

Game::Game(GameType gameType, istream& in) :
	m_board{},
	m_piecesHandle{gameType},
	m_in{in}
{
	// at least two players
	auto result = m_board.TryAddPlayer(Direction::South, m_piecesHandle) &&
		m_board.TryAddPlayer(Direction::North, m_piecesHandle);

	if (!result)
		throw "[Debug]";

	// three players?
	if (result = result && m_board.TryAddPlayer(Direction::West, m_piecesHandle)) {

		if (GameType::TwoPlayers == gameType)
			throw "[Debug]";

		// four players?
		if (result = result && m_board.TryAddPlayer(Direction::East, m_piecesHandle)) {

			if (GameType::TwoPlayers == gameType)
				throw "[Debug]";
		}
	}

	//playerGameLoop(b, ph);
}


Game::~Game()
{
}

void Game::repaint() const
{
	system(SYS_CLEAR);
	m_board.PrintWithLables(cout);
	cout << '\n';
	Game::ShowMainControls();
}

variant<PlayerStats, ErrStr> Game::PlayerGameLoop()
{
	GameMenu::logger.log("Started game");

	repaint();

	// for each player do a turn
	while (!m_board.GameFinished()) {
		m_board.SelectNextPlayer();
		repaint();

		// all actions from now on are applied to selected player
		string line;
		cout << "\nAction: ";
		std::getline(m_in, line);
		auto actionResult = OnInput(Game::ParseInput(line));

		// read until valid input
		while (ActionResult::InvalidInput == actionResult || ActionResult::ImpossibleAction == actionResult) {
			cout << "\nAction: ";
			std::getline(m_in, line);
			actionResult = OnInput(Game::ParseInput(line));
		}


		// player quits
		if (ActionResult::Exit == actionResult) {
			// get selected player
			auto& selectedPlayer = m_board.GetSelectedPlayer().value().get();

			// QuitAll?
			auto parseResult = applyParser(line, Parse::QuitingAll);
			if (std::holds_alternative<Success>(parseResult)) {
				repaint();
				GameMenu::logger.log("Quit active game");
				return Exit("Goodbye");
			}

			// logging
			{
				string msg = "Player ";
				msg += directionToStr(selectedPlayer.GetPawn().GetOrigin());
				msg += " exited game";
			}

			//quit player
			AddQuiter(selectedPlayer.GetPawn().GetOrigin());
			
			m_board.QuitPlayer(selectedPlayer);
			repaint();

			// remaing player is a winner
			if (m_board.GameFinished()) {
				while (m_board.GetPlayerCount()) {
					m_board.SelectNextPlayer();
					auto& selectedPlayer = static_cast<const Board&>(m_board).GetSelectedPlayer().value().get();
					AddWinner(selectedPlayer.GetPawn().GetOrigin());
					
					m_board.QuitPlayer(selectedPlayer);
				}
			}
		}
		// player finish check
		else if (ActionResult::Moved == actionResult) {
			// get selected player
			auto& selectedPlayer = m_board.GetSelectedPlayer().value().get();

			if (m_board.HasPlayerFinished(selectedPlayer)) {
				// logging
				{
					string msg = "Player ";
					msg += directionToStr(selectedPlayer.GetPawn().GetOrigin());
					msg += " finished game";
				}

				AddWinner(selectedPlayer.GetPawn().GetOrigin());
				m_board.QuitPlayer(selectedPlayer);
			}
			repaint();
		}
	}

	// remaing player is a loser
	while (m_board.GetPlayerCount()) {
		m_board.SelectNextPlayer();
		auto& selectedPlayer = static_cast<const Board&>(m_board).GetSelectedPlayer().value().get();
		AddLoser(selectedPlayer.GetPawn().GetOrigin());

		m_board.QuitPlayer(selectedPlayer);
	}

	// final print;
	repaint();
	return std::make_tuple(GetWinners(), GetLosers(), GetQuiters());
}

deque<Direction> Game::GetWinners() const
{
	return m_winners;
}

deque<Direction> Game::GetLosers() const
{
	return m_losers;
}

deque<Direction> Game::GetQuiters() const
{
	return m_quiters;
}

void Game::AddWinner(Direction pawnOrigin)
{
	m_winners.push_back(pawnOrigin);
}

void Game::AddQuiter(Direction pawnOrigin)
{
	m_quiters.push_back(pawnOrigin);
}

void Game::AddLoser(Direction pawnOrigin)
{
	m_losers.push_back(pawnOrigin);
}

ActionResult Game::OnInput(variant<Direction, Place, ErrStr> ioResult)
{
	// print error if any
	if (std::holds_alternative<ErrStr>(ioResult)) {
		const ErrStr& err = std::get<ErrStr>(ioResult);

		repaint();
		cout << err.view() << endl;

		if (err.exit()) {
			return ActionResult::Exit;
		}
		GameMenu::logger.log("Invalid input", Logger::Level::Error);
		return ActionResult::InvalidInput;
	}
	// try moving
	else if (std::holds_alternative<Direction>(ioResult)) {
		auto direction = std::get<Direction>(ioResult);
		
		if (Direction::None == direction)
			throw "[Debug]";

		// try moving
		auto moveResult = Board::MoveResult::Success;
		auto error = m_board.MoveSelected(direction,
			[this, &moveResult](auto effectiveMoveResult, auto&, auto) {
			repaint();
			if (Board::MoveResult::Success != effectiveMoveResult) {
				cout << "Invalid move\n";
			}
			moveResult = effectiveMoveResult;
		});

		// if moving failed completly
		if (Board::MoveResult::Success != moveResult) {
			GameMenu::logger.log("Impossible action", Logger::Level::Error);
			return ActionResult::ImpossibleAction;
		}

		// ...else if sideways move is possible
		if (error.has_value()) {
			if (Board::MoveResult::Success != moveResult)
				throw "[Debug] lambda from MoveSelected should not've been called.";

			auto openSideways = error.value().value();
			return TryMovingDiagonally(direction, openSideways);
		}

		return ActionResult::Moved;
	}
	else {
		auto&[position, direction] = std::get<Place>(ioResult);

		// idea: when direction for wall is choosen repaint the screen
		// with E4, E1 etc.;
		auto actionResult = ActionResult::Placed;

		m_board.PlaceWall(m_piecesHandle, position, direction,
			[this, &actionResult](optional<ErrStr> placeResult) {
			if (!placeResult.has_value()) {
				repaint();
			}
			else {
				repaint();
				auto errMsg = placeResult.value().view(); // todo auto& (or conversion for ErrStr)
				GameMenu::logger.log(errMsg.data(), Logger::Level::Error);
				cout << errMsg << endl;
				actionResult = ActionResult::ImpossibleAction;
			}
		});
		return actionResult;
	}

	GameMenu::logger.log("Invalid input", Logger::Level::Error);
	return ActionResult::InvalidInput;
}

ActionResult Game::TryMovingDiagonally(Direction firstDirection, DirectionChecklist openSideways)
{
	string match = OpenSidewaysToMatch(openSideways);
	SimpleParser parseSideways(match);

	cout << "\nDiagonal move directions: ";
	cout << match << endl;

	string line;
	cout << "\nDirection: ";
	getline(m_in, line);

	// parse input direction
	if (auto result = parseSideways(line)) {
		Direction secondDirection = charToDirection(result.value());
		req(openSideways.test(static_cast<int>(secondDirection)));

		// try moving diagonally
		auto moveResult = Board::MoveResult::Success;

		m_board.MoveSelectedDiag(firstDirection, secondDirection,
			[this, &moveResult](auto effectiveMoveResult, auto&, auto) {
			repaint();
			if (Board::MoveResult::Success != effectiveMoveResult)
				cout << "Invalid move\n";
			moveResult = effectiveMoveResult;
		});

		if (Board::MoveResult::Success != moveResult) {
			GameMenu::logger.log("Move failed", Logger::Level::Error);
			return ActionResult::ImpossibleAction;
		}
	}
	else {
		repaint();
		GameMenu::logger.log("Invalid input", Logger::Level::Error);
		cout << "\nInvalid input.\n";
		return ActionResult::InvalidInput;
	}


	GameMenu::logger.log("Move was successful");
	return ActionResult::Moved;
}

// last
variant<Direction, Place, ErrStr> Game::ParseInput(const string& line, GameState gameState)
{
	//string line;
	//cout << "\nAction: ";
	//std::getline(m_in, line);

	{
		auto parseResult = applyParser(line, Parse::Quiting);
		if (std::holds_alternative<Success>(parseResult))
			return Exit("Goodbye!");
	}

	{
		auto parseResult = applyParser(line, Parse::Placing);
		if (std::holds_alternative<Success>(parseResult))
			gameState = GameState::Placing;
	}

	switch (gameState)
	{
	case GameState::Moving:
	{
		auto parseResult = Game::ParseDirection(line);
		if (std::holds_alternative<Direction>(parseResult))
			return std::get<Direction>(parseResult);
		return std::get<ErrStr>(parseResult);
	}
	case GameState::Placing:
	{
		// yes yes here separete parsePlace
		if (!m_board.CanSelectedPlace()) {
			return ErrStr("No more walls available for selected player.");
		}

		auto parseResult = Game::ParsePlace(); // only here is a param used
		if (std::holds_alternative<Place>(parseResult))
			return std::get<Place>(parseResult);
		return std::get<ErrStr>(parseResult);
	}
	default:
		return ErrStr("Invalid option.");
	}
}

variant<Place, ErrStr> Game::ParsePlace()
{
	string line;
	cout << "\nPlacing wall ...\nDirection: ";
	std::getline(m_in, line);

	auto cancelCheck = applyParser(line, Parse::Quiting);
	if (std::holds_alternative<Success>(cancelCheck))
		return ErrStr("Backing up.");

	auto directionResult = Game::ParseDirection(line);
	if (std::holds_alternative<Direction>(directionResult)) {
		auto direction = std::get<Direction>(directionResult);
		// todo show direction tags based on direction

		// HERE
		system(SYS_CLEAR);
		switch (direction)
		{
		case Direction::North: case Direction::South:
			m_board.PrintWithLablesNorthSouth(cout);
			break;
		case Direction::West: case Direction::East:
			m_board.PrintWithLablesWestEast(cout);
			break;
			break;
		default:
			req(0, "[Debug]");
		}

		auto placeResult = Game::ReadPosition();
		if (std::holds_alternative<ErrStr>(placeResult)) {
			return std::get<ErrStr>(placeResult);
		}

		return Place{ std::get<Position>(placeResult),  direction };
	}
	return ErrStr("Invalid direction");
}

variant<Direction, ErrStr> Game::ParseDirection(const string_view& line)
{
	auto parseValueToDirection = [](ParsedValue<GameState::Moving>& parsedValue, auto) {
		return charToDirection(parsedValue);
	};

	auto parseResult = applyParser<Direction>(line, Parse::Moving, parseValueToDirection);
	if (std::holds_alternative<Direction>(parseResult))
		return std::get<Direction>(parseResult);
	return ErrStr("Invalid direction");
}




variant<Position, ErrStr> Game::ReadPosition()
{
	cout << "\nPosition: ";

	string line;
	static const ErrStr NaN("Invalid number.");

	// row
	cout << "\nrow: ";
	getline(m_in, line);

	auto rowOpt = stringToInteger(line);
	if (!rowOpt.has_value()) {
		return NaN;
	}

	// col
	cout << "\ncol: ";
	getline(m_in, line);

	auto colOpt = stringToInteger(line);
	if (!colOpt.has_value()) {
		return NaN;
	}

	return Position(rowOpt.value(), colOpt.value());
}

void Game::ShowMainControls()
{
	ShowPlaceControls();
	ShowDirectionControls();
	ShowQuitControls();
}

void Game::ShowPlaceControls()
{
	for (char c : string_view{ Parse::placing}) {
		//cout << c << ' ';
		const char str[] = { ' ', c, ' ' , 0 };
		print_green(str);
	}
	cout << "- Place menu\n";
}

void Game::ShowQuitControls()
{
	for (char c : string_view{ Parse::quiting }) {
		//cout << c << ' ';
		const char str[] = { ' ', c, ' ', 0 };
		print_green(str );
	}
	cout << "- Quit Game\n";
}

void Game::ShowDirectionControls()
{
	for (char c : string_view{ Parse::moving }) {
		//cout << c << ' ';
		const char str[] = { ' ', c, ' ', 0 };
		print_green(str );
	}
	cout << "- Indicate move/place direction\n";
}

string Game::OpenSidewaysToMatch(DirectionChecklist openSideways)
{
	string match{};
	for (int itr = 0; itr < openSideways.size(); ++itr) {
		Direction direction = toDir(itr);
		if (openSideways.test(itr)) {
			match += directionToChar(direction);
		}
	}
	return match;
}