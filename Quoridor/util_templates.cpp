#include "util_templates.h"


Err<const char*>::Err(const char* str, bool exit) :
	delete_me{ str },
	m_exit{ exit }
{
}

string_view Err<const char*>::view() const {
	return delete_me.c_str();
}

bool Err<const char*>::exit() const {
	return m_exit;
}

Exit::Exit(const char *str) :
	Err(str, true)
{
	// empty
}