#pragma once

#include "PiecesHandle.h"

using Direction = Piece::Direction;
using DirectionChecklist = Piece::DirectionChecklist;
using DirectionChecklistErrOpt = optional<Err<DirectionChecklist>>;

class Board;

class Player
{
private:
	Player(Pawn& dummyPawn); // use only by dummyPlayer member;
	Player(Pawn&, PiecesHandle& ph);

public:
	static optional<Player> MakePlayer(Direction, PiecesHandle&); // last in this file

	Pawn& AccessPawn();
	const Pawn& GetPawn() const;
	size_t WallCount() const;
	bool HasWalls() const;
	void decrementWallCount();

	OptRef<PlaceableWall> PickWall(const Position&, Direction, PiecesHandle&);

	static Player dummyPlayer;
private:
	size_t m_wallCount;
	ref_wrapper<Pawn> m_pawn;
};
