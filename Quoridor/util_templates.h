#pragma once

#include <string_view>
#include <string>
using std::string_view;
using std::string;

template<typename T>
struct ignore_deduction {
	using type = T;
};

// This is based on dependant types which are
// ignored in template argument deduction.
// type<U>::type is a dependant type (is dependant of U).
template<typename T>
using ignore_deduction_t = typename ignore_deduction<T>::type;

template <typename T>
struct Err {
	Err(const T& value) :
		m_value{ value }
	{}

	T& value() { return m_value; }

private:
	T m_value;
};

template<>
struct Err<const char*> {
	Err(const char* str, bool exit = false);
	string_view view() const;
	bool exit() const;

private:
	bool m_exit;
	const string delete_me; // use something simpler
};

struct Exit : Err<const char*> {
	Exit(const char* str);
};

using ErrStr = Err<const char*>; // fix