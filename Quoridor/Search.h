#pragma once

#include "using_functional.h"
#include "util_templates.h"
#include "GraphInterface.h"
#include <queue>
#include <unordered_set>
using std::queue;
using std::unordered_set;

template<typename T, typename Wrapper>
bool search(const GraphInterface<T, Wrapper>& graph,
	Wrapper start,
	ignore_deduction_t<function<bool(const T&)>> isGoal)
{
	queue<Wrapper> q;
	unordered_set<Wrapper> visited;

	q.push(start);
	visited.insert(start);

	while (!q.empty()) {
		auto current = graph.GetNeighbor(q.front());
		q.pop();

		if (isGoal(current)) {
			return true;
		}
		
		auto neighbors = graph.GetNeighbors(current);
		for (auto& neighborWrapper : neighbors) {
			// if (neighborWrapper) !visited
			if (visited.find(neighborWrapper) == visited.end()) {
				visited.insert(neighborWrapper);
				q.push(neighborWrapper);
			}
		}
	}
	return false;
}