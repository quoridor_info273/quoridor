#pragma once

#include "PiecesHandle.h"
#include "req.hh"
#include "GameState.h"
#include "Board.h"
#include <iostream>
#include <string>
#include <string_view>
#include <tuple>
#include <locale>
#include <deque>
#include <fstream>
#include <istream>


using
	std::ofstream,
	std::istream,
	std::deque,
	std::pair,
	std::tuple,
	std::string,
	std::string_view,
	std::cout,
	std::endl,
	std::cin;

using Place = pair<Position, Direction>;
using PlayerStats = tuple<deque<Direction>, deque<Direction>, deque<Direction>>;

// Used by OnInput to show state in PlayerGameLoop
enum class ActionResult {
	Moved,
	Placed,
	Exit,
	ImpossibleAction,
	InvalidInput
};

class Game
{
public:
	Game(GameType, istream& out = cin);
	~Game();

	// printing
	void repaint() const;
	
	// start
	variant<PlayerStats, ErrStr> PlayerGameLoop();

	// stats
	deque<Direction> GetWinners() const;
	deque<Direction> GetLosers() const;
	deque<Direction> GetQuiters() const;
	void AddWinner(Direction pawnOrigin);
	void AddQuiter(Direction pawnOrigin);
	void AddLoser(Direction pawnOrigin);

	// mutate the board based on input value
	ActionResult OnInput(variant<Direction, Place, ErrStr> ioResult);
public:
	//static utility: parse input to data
	variant<Direction, Place, ErrStr> ParseInput(const string& line, GameState = GameState::Moving);
	variant<Place, ErrStr> ParsePlace();
	variant<Direction, ErrStr> ParseDirection(const string_view&);

	variant<Position, ErrStr> ReadPosition();

	// Show controls
	static void ShowDirectionControls();
	static void ShowQuitControls();
	static void ShowPlaceControls();
	static void ShowMainControls();

private:
	// private utility
	static string OpenSidewaysToMatch(DirectionChecklist openSideways);
	ActionResult TryMovingDiagonally(Direction firstDirection, DirectionChecklist openSideways);

private:
	Board m_board;
	PiecesHandle m_piecesHandle;

	istream& m_in;

	// stats
	deque<Direction> m_winners;
	deque<Direction> m_losers;
	deque<Direction> m_quiters;
};

