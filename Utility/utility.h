#pragma once

#include <optional>
#include <string>
using std::string, std::optional;

#ifdef UTILITY_EXPORTS
#define UTILITY_API __declspec(dllexport)
#else
#define UTILITY_API __declspec(dllimport)
#endif

#define TO_STR(x) TO_STR_EXPANDED(x)
#define TO_STR_EXPANDED(x) #x

bool UTILITY_API isAlpha(char);
optional<int> UTILITY_API stringToInteger(const string&);