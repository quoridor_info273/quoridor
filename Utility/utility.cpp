#include "utility.h"

#include <string_view>
#include <cctype>

using std::string_view;

bool isAlpha(char c)
{
	return c >= 'A' && c <= 'z';
}

optional<int> stringToInteger(const string& str)
{
	//	static std::locale utf8("en_US.UTF8");
	try {
		size_t lastCharIndex;
		int result = std::stoi(str, &lastCharIndex);

		string_view remaining = str;
		remaining = remaining.substr(lastCharIndex, str.size());

		for (char c : remaining) {
			if (!isspace(c)) {
				return {};
			}
		}

		return result;
	}
	catch (...) {
		return {};
	}
}